package it.rentalmanage.view;

import it.rentalmanage.controller.*;
import it.rentalmanage.model.*;

import javax.swing.*;
import java.util.List;
import java.util.Set;

/**
 * Il pannello permette di modificare alcune informazioni di una persona
 * @author nicolapanigucci
 * @see it.rentalmanage.view.FormPerson
 */
public class ModifyPerson extends FormPerson{

    private JTextField tfSurname;
    private JTextField tfName;
    private JTextField tfTelephone;
    private JLabel fiscalCode;
    private JTextField tfAddress;
    private JLabel birthDate;
    private String birth;
    private List<DrivingLicense> allDLicense;

    private IPersonController personController;

    /**
     * Costruttore di ModifyPerson
     * @param prevPanel oggetto di tipo MainFrame
     * @param person oggetto di tipo IPerson. Persona alla quale si vogliono apportare modifiche
     * @param model oggetto di tipo IModel
     * @param allCfset set dei codici fiscali di tutte le persone registrate
     */
    public ModifyPerson(final MainFrame prevPanel, final IPerson person, final IModel model, final Set<String> allCfset) {
        super(prevPanel,model, allCfset);

        this.personController = new PersonController(model, person);

        this.allDLicense = person.getDrivingLicense();
        this.birth = sdf.format(person.getBirthDate());

        panelTitle.add(new JLabel("MODIFY CUSTOMER"));

        this.tfSurname = new JTextField(15);
        c.gridx = 1;
        c.gridy = 0;
        c.insets = padding;
        panelForm.add(tfSurname, c);

        this.tfName = new JTextField(15);
        c.gridx = 1;
        c.gridy = 1;
        c.insets = padding;
        panelForm.add(tfName, c);

        this.fiscalCode = new JLabel(person.getFiscalCode());
        c.gridx = 1;
        c.gridy = 2;
        c.insets = padding;
        panelForm.add(fiscalCode, c);

        this.birthDate = new JLabel(this.birth);
        c.gridx = 1;
        c.gridy = 3;
        c.insets = padding;
        panelForm.add(birthDate, c);

        this.tfAddress = new JTextField(15);
        c.gridx = 1;
        c.gridy = 4;
        c.insets = padding;
        panelForm.add(tfAddress, c);

        this.tfTelephone = new JTextField(15);
        c.gridx = 1;
        c.gridy = 5;
        c.insets = padding;
        panelForm.add(tfTelephone, c);

        this.tfSurname.setText(person.getSurname());
        this.tfName.setText(person.getName());
        this.tfAddress.setText(person.getAddress());
        this.tfTelephone.setText(person.getPhoneNumber());
        allDrivingLicense.setText(allDLicense.toString());

        /**
         * Elimina la persona
         */
        btnRemove.addActionListener(e1 -> {
            int result = JOptionPane.showConfirmDialog(null, "Do you want erase it?", "Warning", JOptionPane.YES_NO_OPTION);

            personController.deletePerson(prevPanel, this, result);
        });

        /**
         * Aggiorna i dati della persona
         */
        btnSaveModify.setText("Modify");
        btnSaveModify.addActionListener(e -> {

            if(checkInput()){

                personController.updatePerson(tfSurname.getText(), tfName.getText(), tfAddress.getText(),
                        tfTelephone.getText(), allDLicense);

                prevPanel.setPanel(new StoragePerson(prevPanel, model));

            }
        });

        /**
         * Aggiunge la patente selezionata alla lista delle patenti possedute e aggiorna
         * la label 'allDrivingLicense'
         */
        btnAddDriveLicense.addActionListener(e -> {
            allDrivingLicense.setText(this.formPersonController.addDriveLicenseToList(allDLicense,
                    (DrivingLicense) cbDrvLicense.getSelectedItem(), this.birth).toString());

            if (allDLicense.size() > 0){
                btnRemoveDriveLicense.setEnabled(true);
            }
        });

        /**
         * Rimuove la patente selezionata dalla lista delle patenti possedute e aggiorna la
         * label 'allDrivingLicense'
         */
        btnRemoveDriveLicense.addActionListener(e1 -> {
            allDrivingLicense.setText(this.formPersonController.removeDriveLicenseFromList(allDLicense, specificDLicenseList,
                    (DrivingLicense) cbDrvLicense.getSelectedItem()).toString());

            if (allDLicense.size() == 0){
                btnRemoveDriveLicense.setEnabled(false);
            }
        });

        prevPanel.setVisibleBtnBackListener(true, e -> prevPanel.setPanel(new ViewPerson(prevPanel, model, person, null)));

    }

    /**
     * Controlla se i dati inseriti sono validi
     * @return true se sono tutti validi
     */
    private boolean checkInput(){

        String surname = tfSurname.getText();
        if(!surname.matches("\\A[A-Za-z'\\\\ ]+\\z")){
            JOptionPane.showMessageDialog(this, "Wrong suname!");
            return false;
        }

        String name = tfName.getText();
        if(!name.matches("\\A[A-Za-z'\\\\ ]+\\z")){
            JOptionPane.showMessageDialog(this, "Wrong name!");
            return false;
        }

        String address = tfAddress.getText();
        if (!address.matches("\\A[\\w'\\.\\ ]+\\z")){
            JOptionPane.showMessageDialog(this, "Wrong format address");
            return false;
        }

        String telephone = tfTelephone.getText();
        if(!telephone.matches("\\A\\d{6,10}\\z")) {
            JOptionPane.showMessageDialog(this, "Telephone number must be 6 to 10 numbers!");
            return false;
        }

        if(allDrivingLicense.getText().isEmpty() || allDrivingLicense.getText().equals("[]")){
            JOptionPane.showMessageDialog(this, "Wrong Driving License");
            return false;
        }

        return true;

    }

}