package it.rentalmanage.view;

import it.rentalmanage.model.IModel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/**
 * Layout della finestra principale del programma RentalManage
 * @author nicolapanigucci
 * @see it.rentalmanage.view.IMainFrame
 */
public class MainFrame extends JFrame implements IMainFrame {

    private JButton btnHome;
    private JButton btnBack;
    private JPanel pannelFoot;
    private JPanel actualPanel = null;
    private Dimension dimBtns;

    /**
     * Costruttore di MainFrame
     * @param model oggetto di tipo IModel
     */
    public MainFrame(final IModel model) {

        this.setSize(730, 530);
        this.setResizable(false);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.setTitle("RentalManage");

        this.getContentPane().setLayout(new BorderLayout());

        WindowListener listener = new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                super.windowClosed(e);
                dispose();
                System.exit(0);
            }
        };

        this.addWindowListener(listener);

        this.dimBtns = new Dimension(55, 55);
        this.pannelFoot = new JPanel(new FlowLayout(FlowLayout.RIGHT));

        this.btnHome = new JButton(new ImageIcon(getClass().getResource("/Images/home.png")));
        this.btnHome.setPreferredSize(dimBtns);
        this.btnHome.setBackground(pannelFoot.getBackground());

        this.btnBack = new JButton(new ImageIcon(getClass().getResource("/Images/btnBack.png")));
        btnBack.setPreferredSize(dimBtns);
        btnBack.setBackground(pannelFoot.getBackground());
        btnBack.setVisible(false);

        setPanel(new HomePage(this, model));

        this.btnHome.addActionListener(e -> setPanel(new HomePage(MainFrame.this, model)));

        pannelFoot.add(btnBack);
        pannelFoot.add(btnHome);

        this.add(pannelFoot, BorderLayout.SOUTH);

        this.setLocationRelativeTo(null);
        this.setVisible(true);

    }

    @Override
    public void setPanel(JPanel newPanel) {

        if (this.actualPanel != null) {
            this.remove(this.actualPanel);
        }

        this.actualPanel = newPanel;

        this.add(actualPanel, BorderLayout.CENTER);

        revalidate();
    }

    @Override
    public void setVisibleBtnBackListener(boolean visible, ActionListener listener) {
        for (ActionListener a : this.btnBack.getActionListeners()) {
            this.btnBack.removeActionListener(a);
        }

        this.btnBack.setVisible(visible);
        this.btnBack.addActionListener(listener);
    }

}