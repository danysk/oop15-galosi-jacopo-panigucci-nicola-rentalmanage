package it.rentalmanage.view;

import it.rentalmanage.controller.*;
import it.rentalmanage.model.*;
import javax.swing.*;
import java.awt.*;

/**
 * Il pannello permette di modificare alcune informazioni di un veicolo
 * @author nicolapanigucci
 * @see it.rentalmanage.view.FormVehicles
 */
public class ModifyVehicle extends FormVehicles {

    private JLabel manufacturer;
    private JLabel model;
    private JLabel carSeats;
    private JLabel numberPlate;
    private JTextField tfRentPrice;
    private JComboBox<DrivingLicense> cbDrvLicense;
    private JLabel lblRentable;
    private JLabel isRentable;

    private IVehicleController vehicleController;

    /**
     * Costruttore di ModifyVehicle
     * @param prevPanel oggetto di tipo MainFrame
     * @param vehicle oggetto di tipo IVehicle. Veicolo al quale si vogliono apportare modifiche
     * @param model oggetto di tipo IModel
     */
    public ModifyVehicle(final MainFrame prevPanel, final IVehicle vehicle, final IModel model) {
        super(prevPanel,model);

        this.vehicleController = new VehicleController(model, vehicle);

        panelTitle.add(new JLabel("MODIFY VEHICLE"));

        this.manufacturer = new JLabel();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 1;
        c.gridy = 0;
        c.insets = otherLines;
        panelForm.add(manufacturer, c);

        this.model = new JLabel();
        this.model.setHorizontalAlignment(JLabel.CENTER);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 3;
        c.gridy = 0;
        c.insets = otherLines;
        panelForm.add(this.model, c);

        this.numberPlate = new JLabel();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 1;
        c.gridy = 1;
        c.insets = otherLines;
        panelForm.add(numberPlate, c);

        this.carSeats = new JLabel();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 3;
        c.gridy = 1;
        c.insets = otherLines;
        panelForm.add(carSeats, c);

        this.tfRentPrice = new JTextField(5);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 2;
        c.gridy = 2;
        c.insets = firstLine;
        panelForm.add(tfRentPrice, c);

        this.cbDrvLicense = new JComboBox<>(DrivingLicense.values());
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 2;
        c.gridy = 3;
        c.insets = otherLines;
        c.anchor = GridBagConstraints.CENTER;
        panelForm.add(cbDrvLicense, c);

        this.lblRentable = new JLabel("Stato :");
        this.lblRentable.setHorizontalAlignment(JLabel.RIGHT);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 1;
        c.gridy = 4;
        c.insets = otherLines;
        panelForm.add(lblRentable, c);

        this.isRentable = new JLabel();
        this.isRentable.setHorizontalAlignment(JLabel.CENTER);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 2;
        c.gridy = 4;
        c.insets = otherLines;
        panelForm.add(isRentable, c);

        this.manufacturer.setText("  " + vehicle.getManufactorer());
        this.tfRentPrice.setText("" + vehicle.getRentPrice());
        this.numberPlate.setText("  " + vehicle.getNumberPlate());
        this.carSeats.setText("  " + vehicle.getVehicleSeats());
        this.model.setText("  " + vehicle.getModel());
        this.cbDrvLicense.setSelectedItem(vehicle.getRequestedLicense());

        if (vehicle.isRentable()){
            isRentable.setText("Rentable");
        }else {
            isRentable.setText("Not Rentable");
        }

        btnPay.setVisible(false);
        btnRemove.setVisible(true);
        btnSaveModify.setText("Modify");

        /**
         * Aggiorna i dati del veicolo
         */
        btnSaveModify.addActionListener(e -> {
            if(checkInput()){

                vehicleController.updateVehicle(Integer.parseInt(tfRentPrice.getText()), (DrivingLicense) cbDrvLicense.getSelectedItem(),
                        vehicle.getInsuranceCost() ,vehicle.getInsuranceExpiring(), vehicle.getVehicleTaxCost(),
                        vehicle.getVehicleTaxDate(), vehicle.getMotTestCost(), vehicle.getMotTestDate());

                prevPanel.setPanel(new StorageVehicle(prevPanel, model));
            }
        });

        /**
         * Elimina il veicolo
         */
        btnRemove.addActionListener(e -> {
            int result = JOptionPane.showConfirmDialog(null, "Do you want erase it?", "Warning", JOptionPane.YES_NO_OPTION);

            vehicleController.deleteVehicle(prevPanel, this, result);
        });

        prevPanel.setVisibleBtnBackListener(true, e -> prevPanel.setPanel(new ViewVehicle(prevPanel, vehicle, model)));

    }

    /**
     * Controlla che i dati inseriti siano validi
     * @return true se sono validi
     */
    private boolean checkInput(){

        String rentPrice = tfRentPrice.getText();
        if(!rentPrice.matches("\\A\\d+\\z") || Integer.parseInt(rentPrice) == 0) {
            JOptionPane.showMessageDialog(this, "Wrong rent price!");
            return false;
        }

        if(cbDrvLicense.getSelectedIndex() == -1){
            JOptionPane.showMessageDialog(this, "Wrong license!");
            return false;
        }

        return true;
    }

}
