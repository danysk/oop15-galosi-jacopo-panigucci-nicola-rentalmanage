package it.rentalmanage.view;

import it.rentalmanage.model.IVehicle;

import javax.swing.table.TableModel;
import java.util.Map;

/**
 * Implementa il metodo che estrae la lista dei veicoli registrati
 * @author nicolapanigucci
 * @see javax.swing.table.TableModel
 */
public interface ICustomTableModelVehicle extends TableModel{

    /**
     * Estrae la lista dei veicoli
     * @param vehicleList mappa dei veicoli registrati
     */
    void setElement(Map<String,IVehicle> vehicleList);
}
