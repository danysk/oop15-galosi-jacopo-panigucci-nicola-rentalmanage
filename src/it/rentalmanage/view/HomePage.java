package it.rentalmanage.view;

import it.rentalmanage.model.*;

import javax.swing.*;
import java.awt.*;

/**
 * Imposta il layout del primo pannello mostrato all'avvio del programma
 * @author nicolapanigucci
 */
public class HomePage extends JPanel {

    private JLabel lblLogo;
    private JButton btnManageClients;
    private JButton btnManageVehicles;
    private JPanel panLogo;
    private JPanel panButtons;

    /**
     * Costruttore di HomePage
     * @param prevPanel oggetto di tipo MainFrame
     * @param model oggetto di tipo IModel
     */
    public HomePage(final MainFrame prevPanel, final IModel model){

        this.setLayout(new BorderLayout());


        this.lblLogo = new JLabel(new ImageIcon(getClass().getResource("/Images/Logo.png")));
        this.lblLogo.setPreferredSize(new Dimension(400, 300));

        this.btnManageClients = new JButton("Manage Person");
        this.btnManageClients.setPreferredSize(new Dimension(this.btnManageClients.getPreferredSize().width * 2,
                this.btnManageClients.getPreferredSize().height * 2));

        this.btnManageVehicles = new JButton("Manage Vehicles");
        this.btnManageVehicles.setPreferredSize(new Dimension(this.btnManageVehicles.getPreferredSize().width * 2,
                this.btnManageVehicles.getPreferredSize().height * 2));

        this.btnManageClients.addActionListener(e -> prevPanel.setPanel(new StoragePerson(prevPanel, model)));

        this.btnManageVehicles.addActionListener(e -> prevPanel.setPanel(new StorageVehicle(prevPanel, model)));

        this.panLogo = new JPanel(new FlowLayout(FlowLayout.CENTER));
        this.panButtons = new JPanel(new FlowLayout(FlowLayout.CENTER));

        this.panLogo.add(this.lblLogo);
        this.panButtons.add(this.btnManageClients);
        this.panButtons.add(this.btnManageVehicles);

        this.add(this.panLogo, BorderLayout.NORTH);
        this.add(this.panButtons, BorderLayout.CENTER);

        prevPanel.setVisibleBtnBackListener(false, null);

    }

}
