package it.rentalmanage.view;

import it.rentalmanage.controller.*;
import it.rentalmanage.model.*;
import it.rentalmanage.model.filemanager.JSonFileManager;

import javax.swing.*;
import java.text.ParseException;
import java.util.*;

/**
 * Pannello che permette di registrare una nuova persona
 * @author nicolapanigucci
 * @see it.rentalmanage.view.FormPerson
 */
public class AddPerson extends FormPerson {

    private JTextField tfSurname;
    private JTextField tfName;
    private JTextField tfFiscalCode;
    private JTextField tfbirthDate;
    private JTextField tfAddress;
    private JTextField tfTelephone;
    private Set<String> allCfSet;
    private List<DrivingLicense> allDLicense;

    /**
     * Costruttore di AddPerson
     * @param prevPanel oggetto di tipo MainFrame
     * @param model oggetto di tipo IModel
     * @param allCfSet set dei codici fiscali delle persona registrate
     */
    public AddPerson(final MainFrame prevPanel, final IModel model, final Set<String> allCfSet) {
        super(prevPanel, model, allCfSet);

        this.panelTitle.add(new JLabel("ADD CUSTOMER"));

        this.allCfSet = allCfSet;
        this.allDLicense = new LinkedList<>();

        this.tfSurname = new JTextField(15);
        c.gridx = 1;
        c.gridy = 0;
        c.insets = padding;
        panelForm.add(tfSurname, c);

        this.tfName = new JTextField(15);
        c.gridx = 1;
        c.gridy = 1;
        c.insets = padding;
        panelForm.add(tfName, c);

        this.tfFiscalCode = new JTextField(15);
        c.gridx = 1;
        c.gridy = 2;
        c.insets = padding;
        panelForm.add(tfFiscalCode,c);

        this.tfbirthDate = new JTextField(15);
        tfbirthDate.setText("dd/MM/yyyy");
        c.gridx = 1;
        c.gridy = 3;
        c.insets = padding;
        panelForm.add(tfbirthDate, c);

        this.tfAddress = new JTextField(15);
        c.gridx = 1;
        c.gridy = 4;
        c.insets = padding;
        panelForm.add(tfAddress, c);

        this.tfTelephone = new JTextField(15);
        c.gridx = 1;
        c.gridy = 5;
        c.insets = padding;
        panelForm.add(tfTelephone, c);

        /**
         * Salva la persona se i campi inseriti sono corretti
         */
        btnSaveModify.setText("Save");
        btnSaveModify.addActionListener(e1 -> {
            if(checkInput()){
                try {

                    Date bDate = sdf.parse(tfbirthDate.getText());

                    IPerson person = new Person(new LinkedList<IRentedVehiclePeriod>(), tfFiscalCode.getText(), tfTelephone.getText(),
                            bDate, tfAddress.getText(), tfSurname.getText(), tfName.getText(), allDLicense);
                    IAddPersonController controller = new AddPersonController(new JSonFileManager(), model);
                    controller.writePerson(person);

                    prevPanel.setPanel(new StoragePerson(prevPanel, model));

                } catch (ParseException e) {

                }

            }
        });

        /**
         * Aggiunge la patente selezionata alla lista delle patenti possedute e aggiorna
         * la label 'allDrivingLicense'
         */
        btnAddDriveLicense.addActionListener(e -> {
            allDrivingLicense.setText(this.formPersonController.addDriveLicenseToList(allDLicense,
                    (DrivingLicense) cbDrvLicense.getSelectedItem(), tfbirthDate.getText()).toString());

            if (allDLicense.size() > 0){
                btnRemoveDriveLicense.setEnabled(true);
            }
        });

        if (allDLicense.size() == 0){
            btnRemoveDriveLicense.setEnabled(false);
        }

        /**
         * Rimuove la patente selezionata dalla lista delle patenti possedute e aggiorna la
         * label 'allDrivingLicense'
         */
        btnRemoveDriveLicense.addActionListener(e1 -> {
            allDrivingLicense.setText(this.formPersonController.removeDriveLicenseFromList(allDLicense, specificDLicenseList,
                    (DrivingLicense) cbDrvLicense.getSelectedItem()).toString());

            if (allDLicense.size() == 0){
                btnRemoveDriveLicense.setEnabled(false);
            }
        });

        btnRemove.setVisible(false);

        prevPanel.setVisibleBtnBackListener(true, e -> prevPanel.setPanel(new StoragePerson(prevPanel, model)));

    }

    /**
     * Controlla se i dai inseriti sono validi
     * @return true se sono tutti validi
     */
    private boolean checkInput(){

        String surname = tfSurname.getText();
        if(!surname.matches("\\A[A-Za-z'\\\\ ]+\\z")){
            JOptionPane.showMessageDialog(this, "Wrong suname!");
            return false;
        }

        String name = tfName.getText();
        if(!name.matches("\\A[A-Za-z'\\\\ ]+\\z")){
            JOptionPane.showMessageDialog(this, "Wrong name!");
            return false;
        }

        String fiscalCode = tfFiscalCode.getText();
        if (!fiscalCode.matches("\\A[A-Z0-9]{16}\\z")){
            JOptionPane.showMessageDialog(this, "Wrong fiscal code! Only capital characters and numbers");
            return false;
        }

        if(allCfSet.contains(fiscalCode)){
            JOptionPane.showMessageDialog(this, "Fiscal code already used!");
            return false;
        }

        if (!checkOnDate.check16YearsOld(tfbirthDate.getText())){
            return false;
        }

        String address = tfAddress.getText();
        if (!address.matches("\\A[\\w'\\.\\ ]+\\z")){
            JOptionPane.showMessageDialog(this, "Wrong format address");
            return false;
        }

        String telephone = tfTelephone.getText();
        if(!telephone.matches("\\A\\d{6,10}\\z")) {
            JOptionPane.showMessageDialog(this, "Telephone number must be 6 to 10 numbers!");
            return false;
        }

        if(allDrivingLicense.getText().isEmpty() || allDrivingLicense.getText().equals("[]")){
            JOptionPane.showMessageDialog(this, "Wrong Driving License");
            return false;
        }

        return true;

    }

}
