package it.rentalmanage.view;

import it.rentalmanage.model.IVehicle;
import it.rentalmanage.model.IModel;
import it.rentalmanage.model.IPerson;
import it.rentalmanage.model.IRentedVehiclePeriod;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * Questo pannello mostra tutti i dati di una persona registrata
 * @author nicolapanigucci
 * @see it.rentalmanage.view.FormPerson
 */
public class ViewPerson extends FormPerson {

    private Map<String, IVehicle> vehicleMap;
    private List<IVehicle> listRentableVehicles;

    private JLabel surname;
    private JLabel name;
    private JLabel cf;
    private JLabel address;
    private JLabel telephone;
    private JLabel bDay;
    private JLabel lblallRentedVehicle;

    private JButton btnRentVehicle;
    private JButton btnViewAllRentedVehicle;

    /**
     * Costruttore di ViewPerson
     * @param prevPanel oggetto di MainFrame
     * @param model oggetto di IModel
     * @param person oggetto di IPerson.
     * @param allCfSet set dei codici fiscali di tutte le persone registrate
     */
    public ViewPerson(final MainFrame prevPanel, final IModel model, final IPerson person, final Set<String> allCfSet) {
        super(prevPanel, model, allCfSet);

        panelTitle.add(new JLabel("DETAILS PERSON"));

        this.surname = new JLabel();
        c.gridx = 1;
        c.gridy = 0;
        c.insets = padding;
        panelForm.add(surname, c);

        this.name = new JLabel();
        c.gridx = 1;
        c.gridy = 1;
        c.insets = padding;
        panelForm.add(name, c);

        this.cf = new JLabel();
        c.gridx = 1;
        c.gridy = 2;
        c.insets = padding;
        panelForm.add(cf, c);

        this.bDay = new JLabel();
        c.gridx = 1;
        c.gridy = 3;
        c.insets = padding;
        panelForm.add(bDay, c);

        this.address = new JLabel();
        c.gridx = 1;
        c.gridy = 4;
        c.insets = padding;
        panelForm.add(address, c);

        this.telephone = new JLabel();
        c.gridx = 1;
        c.gridy = 5;
        c.insets = padding;
        panelForm.add(telephone, c);

        this.lblallRentedVehicle = new JLabel("All rented vehicle :");
        this.lblallRentedVehicle.setHorizontalAlignment(JLabel.RIGHT);
        c.gridx = 0;
        c.gridy = 7;
        c.insets = padding;
        c.anchor = GridBagConstraints.EAST;
        panelForm.add(lblallRentedVehicle, c);

        this.btnViewAllRentedVehicle = new JButton("View");
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 1;
        c.gridy = 7;
        c.insets = padding;
        panelForm.add(btnViewAllRentedVehicle, c);

        surname.setText(person.getSurname());
        name.setText(person.getName());
        cf.setText(person.getFiscalCode());
        bDay.setText(sdf.format(person.getBirthDate()));
        address.setText(person.getAddress());
        telephone.setText(person.getPhoneNumber());
        allDrivingLicense.setText(person.getDrivingLicense().toString());

        cbDrvLicense.setVisible(false);
        btnAddDriveLicense.setVisible(false);
        btnRemoveDriveLicense.setVisible(false);
        btnRemove.setVisible(false);

        /**
         * Mostra lo storico delle auto noleggiate dal cliente
         */
        btnViewAllRentedVehicle.addActionListener(e2 -> prevPanel.setPanel(new RentalHistorianPerson(prevPanel, model, person)));

        if (model.getAllPersonsHistory().size()==0 || !model.getAllPersonsHistory().containsKey(person.getFiscalCode())){
            btnViewAllRentedVehicle.setEnabled(false);
        }

        btnRentVehicle = new JButton("Rent a Vehicle");
        btnRentVehicle.setEnabled(true);
        panBtnAddVehicle.add(btnRentVehicle);


        /**
         * Controllo se ci sono veicoli noleggiabili
         */
        vehicleMap = model.getAllCar();
        listRentableVehicles = new ArrayList<>();
        Iterator<Map.Entry<String,IVehicle>> iterator = vehicleMap.entrySet().iterator();
        while (iterator.hasNext()){
            Map.Entry<String,IVehicle> entry = iterator.next();
            IVehicle value = entry.getValue();

            if (value.isRentable()){ //requisito necessario
                listRentableVehicles.add(value);
            }
        }

        if (listRentableVehicles.size() == 0){
            btnRentVehicle.addActionListener(e -> JOptionPane.showMessageDialog(this, "There aren't any rentable vehicles!"));
        }else {
            /**
             * Mostra le auto noleggiabili
             */
            btnRentVehicle.addActionListener(e -> prevPanel.setPanel(new RentVehicle(prevPanel, model, person, listRentableVehicles)));
        }

        /**
         * Controllo se una persona ha già un auto in noleggio
         */
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        for (IRentedVehiclePeriod rentedCarPeriod : person.getAllRentedVehicles()){
            if (rentedCarPeriod.getEndDate().after(calendar.getTime())){
                btnRentVehicle.setEnabled(false);
            }
        }

        btnSaveModify.setText("Modify");

        /**
         * Mostra il pannello per modificare i dati della persona
         */
        btnSaveModify.addActionListener(e1 -> prevPanel.setPanel(new ModifyPerson(prevPanel, person, model, allCfSet)));

        prevPanel.setVisibleBtnBackListener(true,e -> prevPanel.setPanel(new StoragePerson(prevPanel, model)));
    }



}
