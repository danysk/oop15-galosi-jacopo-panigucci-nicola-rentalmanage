package it.rentalmanage.view;

import it.rentalmanage.model.IVehicle;
import it.rentalmanage.model.IModel;
import it.rentalmanage.model.IPerson;

import javax.swing.*;
import java.awt.*;
import java.util.List;

/**
 * Imposta il layout del pannello che mostra i veicoli noleggiabili
 * @author nicolapanigucci
 */
public class RentVehicle extends JPanel {

    private JLabel lblRentableVehicles;
    private JPanel panelDetails;
    private GridBagConstraints gbc;

    /**
     * Costruttore di RentVehicle
     * @param prevPanel oggetto di tipo MainFrame
     * @param model oggetto di tpo IModel
     * @param person oggetto di tipo IPerson. Persona che vuole noleggiare un veicolo
     * @param vehicleList lista dei veicoli noleggiabili
     */
    public RentVehicle(final MainFrame prevPanel, final IModel model, final IPerson person, final List<IVehicle> vehicleList){

        this.setLayout(new GridBagLayout());

        lblRentableVehicles = new JLabel("RENTABLE VEHICLES");

        panelDetails = new JPanel(new FlowLayout());
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 1.0;
        gbc.fill = GridBagConstraints.HORIZONTAL;

        panelDetails.add(lblRentableVehicles, JLabel.CENTER);

        this.add(panelDetails, gbc);

        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.fill = GridBagConstraints.BOTH;

        this.add(new TableRentVehicle(prevPanel, person, model, vehicleList), gbc);

        prevPanel.setVisibleBtnBackListener(true, e -> prevPanel.setPanel(new ViewPerson(prevPanel, model, person, null)));

    }
}
