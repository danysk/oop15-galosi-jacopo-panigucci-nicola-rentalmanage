package it.rentalmanage.view;

import it.rentalmanage.model.*;

/**
 * Mostra la tabella TablePerson nel pannello e imposta setta l'evento al bottone
 * @author nicolapanigucci
 * @see it.rentalmanage.view.Storage
 */
public class StoragePerson extends Storage{

    /**
     * Costruttore di StoragePerson
     * @param prevPanel oggetto di tipo MainFrame
     * @param model oggetto di tipo IModel
     */
    public StoragePerson(final MainFrame prevPanel, final IModel model){
        super();

        setAddListener(e -> prevPanel.setPanel(new AddPerson(prevPanel, model, model.getAllPersons().keySet())));
        setTable(new TablePerson(prevPanel, model));

        prevPanel.setVisibleBtnBackListener(true, e -> prevPanel.setPanel(new HomePage(prevPanel, model)));
    }
}
