package it.rentalmanage.view;

import it.rentalmanage.model.IModel;

/**
 * Mostra la tabella TableVehicle nel pannello e imposta setta l'evento al bottone
 * @author nicolapanigucci
 * @see it.rentalmanage.view.Storage
 */
public class StorageVehicle extends Storage {

    /**
     * Costruttore di StorageVehicle
     * @param prevPanel oggetto di tipo MainFrame
     * @param model oggetto di tipo IModel
     */
    public StorageVehicle(final MainFrame prevPanel, final IModel model){
        super();

        setAddListener(e -> prevPanel.setPanel(new AddVehicle(prevPanel, model, model.getAllCar().keySet())));
        setTable(new TableVehicle(prevPanel, model));

        prevPanel.setVisibleBtnBackListener(true, e -> prevPanel.setPanel(new HomePage(prevPanel, model)));
    }

}
