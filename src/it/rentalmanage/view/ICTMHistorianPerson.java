package it.rentalmanage.view;

import it.rentalmanage.model.IPerson;

import javax.swing.table.TableModel;
import java.util.Map;

/**
 * Implementa il metodo per estrarre la lista delle auto noleggiate da un cliente
 * @author nicolapanigucci
 * @see javax.swing.table.TableModel
 */
public interface ICTMHistorianPerson extends TableModel{

    /**
     * Estrae la lista delle auto noleggiate da 'person'
     * @param historianPerson mappa dello storico delle persone
     * @param person oggetto di tipo IPerson. Cliente di cui si vuole visualizzare lo storico
     */
    void setElement(Map<String, IPerson> historianPerson, IPerson person);
}
