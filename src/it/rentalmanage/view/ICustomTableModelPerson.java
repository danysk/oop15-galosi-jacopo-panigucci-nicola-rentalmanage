package it.rentalmanage.view;

import it.rentalmanage.model.IPerson;

import javax.swing.table.TableModel;
import java.util.Map;

/**
 * Implementa il metodo che estrae la lista delle persone registrate
 * @author nicolapanigucci
 * @see javax.swing.table.TableModel
 */
public interface ICustomTableModelPerson extends TableModel {

    /**
     * Estrae la lista delle persone
     * @param personList mappa delle persone registrate
     */
    void setElement(Map<String ,IPerson> personList);
}
