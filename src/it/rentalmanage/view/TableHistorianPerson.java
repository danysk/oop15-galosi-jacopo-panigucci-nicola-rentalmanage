package it.rentalmanage.view;

import it.rentalmanage.controller.*;
import it.rentalmanage.model.IModel;
import it.rentalmanage.model.IPerson;
import it.rentalmanage.model.IRentedVehiclePeriod;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.*;
import java.util.List;

/**
 * Tabella che mostra lo storico dei veicoli noleggiati da una cliente
 * @author nicolapanigucci
 */
public class TableHistorianPerson extends JScrollPane {

    private List<IRentedVehiclePeriod> rentedVehiclePeriodList;
    private JTable tbHistorian;
    private ITableHistorianPersonController tableHistorianPersonController;
    private ICTMHistorianPerson ctmHistorianPerson;

    /**
     * Costruttore di TableHistorianPerson
     * @param prevPanel oggetto di tipo MainFrame
     * @param model oggetto di tipo IModel
     * @param person oggetto di tipo IPerson. Persona di cui si vuole visualizzare lo storico
     */
    public TableHistorianPerson(final MainFrame prevPanel, final IModel model, final IPerson person){

        this.ctmHistorianPerson = new CTMHistorianPerson(model);
        this.tbHistorian = new JTable(ctmHistorianPerson);
        this.tableHistorianPersonController = new TableHistorianPersonController(model, ctmHistorianPerson);
        tableHistorianPersonController.showHistorianPerson(person);
        tbHistorian.setFillsViewportHeight(true);

        this.setViewportView(tbHistorian);

        Iterator<Map.Entry<String,IPerson>> iterator = model.getAllPersonsHistory().entrySet().iterator();
        while (iterator.hasNext()){

            Map.Entry<String,IPerson> entry = iterator.next();
            String key = entry.getKey();
            if (key.equals(person.getFiscalCode())){
                this.rentedVehiclePeriodList = (List<IRentedVehiclePeriod>) person.getAllRentedVehicles();
            }
        }

        /**
         * Gestisce il doppio click su una riga della tabella.
         * Permette di registrare la restituzione di un veicolo
         */
        tbHistorian.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);

                int valueRow = tbHistorian.getSelectedRow();
                if((valueRow!=-1) && (e.getClickCount() == 2) &&
                        !model.getCarFromNumPlateHistory(rentedVehiclePeriodList.get(valueRow).getNumberPlate()).isRentable()){
                    int result = JOptionPane.showOptionDialog(null, "Return a vehicle?", "Return...", JOptionPane.YES_NO_OPTION,
                            JOptionPane.INFORMATION_MESSAGE, null, null, null);

                    IRestitution restitution = new Restitution(model, person, rentedVehiclePeriodList.get(valueRow));
                    restitution.restitution(result);

                    prevPanel.setPanel(new ViewPerson(prevPanel, model, person, null));
                }
            }
        });

        /**
         * Permette di colorare i valori di ogni cella della tabella
         */
        tbHistorian.setDefaultRenderer(Object.class, (table, value, isSelected, hasFocus, row, column) -> {

            JLabel label = new JLabel(value.toString());
            label.setOpaque(true);

            label.setBackground(Color.WHITE);

            if (isSelected){
                label.setBackground(Color.lightGray);

            }else {
                label.setBackground(Color.WHITE);
            }

            IRestitution checkResitution = new Restitution(model, person, rentedVehiclePeriodList.get(row));

            if (!checkResitution.isDeliveredVehicle()){
                label.setForeground(Color.RED);
            }else {
                label.setForeground(Color.GREEN);
            }
            return label;

        });

        revalidate();

    }
}
