package it.rentalmanage.view;

/**
 * Definisce i metodi che permettono di visualizzare JOptionPane il cui testo è definito dai controller
 * @author nicolapanigucci
 */
public interface IFormPerson {

    /**
     * Mostra a video un JOptionPane con il quale l'utente deve interagire
     * @param text testo che il JOptionPane deve mostrare
     * @return scelta dell'utente (YES, NO)
     */
    int showOptionDialog(String text);

    /**
     * Mostra a video un semplice messaggio di errore
     * @param text testo che il JOptionPane deve mostrare
     */
    void message(String text);
}
