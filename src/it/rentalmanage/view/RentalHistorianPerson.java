package it.rentalmanage.view;

import it.rentalmanage.model.IModel;
import it.rentalmanage.model.IPerson;

import javax.swing.*;
import java.awt.*;

/**
 * Imposta il layout del pannello che mostra lo storico dei veicoli noleggiati da un cliente
 * @author nicolapanigucci
 */
public class RentalHistorianPerson extends JPanel {

    private GridBagConstraints gb;

    /**
     * Costruttore di RentalHistorianPerson
     * @param prevPanel oggetto di tipo MainFrame
     * @param model oggetto di tipo IModel
     * @param person oggetto di tipo IPerson. Cliente di cui si vuole conoscere lo storico delle auto noleggiate
     */
    public RentalHistorianPerson(final MainFrame prevPanel, final IModel model, final IPerson person){
        this.setLayout(new GridBagLayout());

        gb = new GridBagConstraints();
        gb.gridx = 0;
        gb.gridy = 0;
        gb.weightx = 1.0;
        gb.weighty = 1.0;
        gb.fill = GridBagConstraints.BOTH;

        this.add(new TableHistorianPerson(prevPanel, model, person), gb);

        prevPanel.setVisibleBtnBackListener(true, e -> prevPanel.setPanel(new ViewPerson(prevPanel, model, person, null)));

    }
}
