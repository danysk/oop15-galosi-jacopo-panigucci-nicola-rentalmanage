package it.rentalmanage.view;

import it.rentalmanage.model.IVehicle;

import javax.swing.table.TableModel;
import java.util.Map;

/**
 * Implementa il metodo per estrarre la lista dei clienti che hanno noleggiato un veicolo
 * @author nicolapanigucci
 * @see javax.swing.table.TableModel
 */
public interface ICTMHistorianVehicle extends TableModel{

    /**
     * Estrae la lista delle persone che hanno noleggiato 'vehicle'
     * @param vehicleMap mappa dei veicoli
     * @param vehicle oggetto di tipo IVehicle. veicolo di cui si vuole visualizzare lo storico
     */
    void setElement(Map<String, IVehicle> vehicleMap, IVehicle vehicle);
}
