package it.rentalmanage.view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * Imposta il layout generale dei pannelli StoragePerson e StorageVehicle
 * @author nicolapanigucci
 */
public class Storage extends JPanel implements IStorage {

    private JButton btnAdd;
    private JPanel pnNorth;
    private GridBagConstraints cnst;

    /**
     * Costruttore di Storage
     */
    public Storage(){
        this.setLayout(new GridBagLayout());

        this.pnNorth = new JPanel(new FlowLayout(FlowLayout.LEFT));

        this.cnst = new GridBagConstraints();
        this.cnst.gridx = 0;
        this.cnst.gridy = 0;
        this.cnst.weightx = 1.0;
        this.cnst.fill = GridBagConstraints.HORIZONTAL;

        this.btnAdd = new JButton(new ImageIcon(getClass().getResource("/Images/add.png")));
        this.btnAdd.setPreferredSize(new Dimension(55, 55));
        this.btnAdd.setBackground(pnNorth.getBackground());

        pnNorth.add(this.btnAdd);

        this.add(pnNorth, this.cnst);
    }

    @Override
    public void setTable(JComponent component) {
        this.cnst = new GridBagConstraints();
        this.cnst.gridx = 0;
        this.cnst.gridy = 1;
        this.cnst.weightx = 1.0;
        this.cnst.weighty = 1.0;
        this.cnst.fill = GridBagConstraints.BOTH;

        this.add(component, this.cnst);
    }

    @Override
    public void setAddListener(ActionListener listener) {
        this.btnAdd.addActionListener(listener);
    }
}
