package it.rentalmanage.view;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * Implementa il metodo che gestisce il cambio di pannello del programma
 * @author nicolapanigucci
 */
public interface IMainFrame {

    /**
     * Immposta il nuovo pannello
     * @param newPanel nuovo pannello
     */
    void setPanel(JPanel newPanel);

    /**
     * Abilita il bottone e gestisce l'evento
     * @param visible  stato del bottone
     * @param listener evento del bottone
     */
    void setVisibleBtnBackListener(boolean visible, ActionListener listener);
}
