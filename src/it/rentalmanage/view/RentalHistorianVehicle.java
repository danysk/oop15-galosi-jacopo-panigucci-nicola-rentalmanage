package it.rentalmanage.view;

import it.rentalmanage.model.IVehicle;
import it.rentalmanage.model.IModel;

import javax.swing.*;
import java.awt.*;

/**
 * Imposta il layout del pannello che mostra lo storico dei clienti che hanno noleggiato un veicolo
 * @author nicolapanigucci
 */
public class RentalHistorianVehicle extends JPanel{

    private GridBagConstraints gb;

    /**
     * Costruttore di RentalHistorianVehicle
     * @param prevPanel oggetto di tipo MainFrame
     * @param model oggetto di tipo IModel
     * @param vehicle oggetto di tipo IVehicle. Veicolo di cui si vuole conocere lo storico dei clienti che lo hanno
     *                noleggiato
     */
    public RentalHistorianVehicle(final MainFrame prevPanel, final IModel model, final IVehicle vehicle){
        this.setLayout(new GridBagLayout());

        gb = new GridBagConstraints();
        gb.gridx = 0;
        gb.gridy = 0;
        gb.weightx = 1.0;
        gb.weighty = 1.0;
        gb.fill = GridBagConstraints.BOTH;

        this.add(new TableHistorianVehicle(prevPanel, model, vehicle), gb);

        prevPanel.setVisibleBtnBackListener(true, e -> prevPanel.setPanel(new ViewVehicle(prevPanel, vehicle, model)));

    }

}
