package it.rentalmanage.view;

import it.rentalmanage.controller.CheckOnDate;
import it.rentalmanage.controller.FormPersonController;
import it.rentalmanage.controller.ICheckOnDate;
import it.rentalmanage.controller.IFormPersonController;
import it.rentalmanage.model.DrivingLicense;
import it.rentalmanage.model.IModel;

import javax.swing.*;
import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

/**
 * Imposta il layout della form dei pannelli che aggiungono, modificano e visualizzano i dati di una persona
 * @author nicolapanigucci
 * @see it.rentalmanage.view.IFormPerson
 */
public class FormPerson extends JPanel implements IFormPerson {

    protected SimpleDateFormat sdf;

    private JLabel lblSurname;
    private JLabel lblName;
    private JLabel lblTelephone;
    private JLabel lblFiscalCode;
    private JLabel lblAddress;
    private JLabel lblBirthDate;
    private JLabel lblDrivingLicense;
    protected JLabel allDrivingLicense;

    protected JButton btnAddDriveLicense;
    protected JButton btnRemoveDriveLicense;
    protected JButton btnSaveModify;
    protected JButton btnRemove;

    protected JComboBox<DrivingLicense> cbDrvLicense;

    private JPanel panelBtn;
    protected JPanel panelTitle;
    protected JPanel panelForm;
    protected JPanel panBtnAddVehicle;

    private GridBagLayout gb;
    protected Insets padding;
    protected GridBagConstraints c;

    protected List specificDLicenseList;
    protected IFormPersonController formPersonController;
    protected ICheckOnDate checkOnDate;

    /**
     * Costruttore di FormPerson
     * @param prevPanel oggetto di tipo MainFrame
     * @param model oggetto di tipo IModel
     * @param allCfSet set dei codici fiscali di tutte le persone registrate
     */
    public FormPerson(final MainFrame prevPanel, final IModel model, final Set<String> allCfSet){

        this.setLayout(new BorderLayout());

        this.sdf = new SimpleDateFormat("dd/MM/yyyy");
        this.sdf.setLenient(false);

        this.specificDLicenseList = Arrays.asList(DrivingLicense.B, DrivingLicense.C, DrivingLicense.CE, DrivingLicense.D, DrivingLicense.DE);
        this.formPersonController = new FormPersonController(FormPerson.this);
        this.checkOnDate = new CheckOnDate(FormPerson.this);

        btnSaveModify = new JButton();
        btnRemove = new JButton("Delete");

        panelTitle = new JPanel(new FlowLayout(FlowLayout.CENTER));
        panelForm = new JPanel();

        gb = new GridBagLayout();
        gb.columnWidths = new int[]{0, 196, 0, 0};
        gb.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
        gb.columnWeights = new double[]{1.0, 0.0, 1.0, Double.MIN_VALUE};
        gb.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        panelForm.setLayout(gb);

        panelBtn = new JPanel(new FlowLayout(FlowLayout.RIGHT));

        c = new GridBagConstraints();
        padding = new Insets(20, 20, 0, 0);

        this.lblSurname = new JLabel("Surname :");
        c.gridx = 0;
        c.gridy = 0;
        c.insets = padding;
        c.anchor = GridBagConstraints.EAST;
        panelForm.add(this.lblSurname, c);

        this.lblName = new JLabel("Name :");
        c.gridx = 0;
        c.gridy = 1;
        c.insets = padding;
        panelForm.add(this.lblName, c);

        this.lblFiscalCode = new JLabel("Fiscal Code :");
        c.gridx = 0;
        c.gridy = 2;
        c.insets = padding;
        panelForm.add(this.lblFiscalCode, c);

        this.lblBirthDate = new JLabel("Birth Date :");
        c.gridx = 0;
        c.gridy = 3;
        c.insets = padding;
        panelForm.add(this.lblBirthDate, c);

        this.lblAddress = new JLabel("Address :");
        c.gridx = 0;
        c.gridy = 4;
        c.insets = padding;
        panelForm.add(this.lblAddress, c);

        this.lblTelephone = new JLabel("Telephone :");
        c.gridx = 0;
        c.gridy = 5;
        c.insets = padding;
        panelForm.add(this.lblTelephone, c);

        this.lblDrivingLicense = new JLabel("Driving License :");
        c.gridx = 0;
        c.gridy = 6;
        c.insets = padding;
        panelForm.add(this.lblDrivingLicense, c);

        this.allDrivingLicense = new JLabel();
        c.gridx = 1;
        c.gridy = 6;
        c.insets = padding;
        c.anchor = GridBagConstraints.CENTER;
        panelForm.add(this.allDrivingLicense, c);

        this.btnAddDriveLicense = new JButton("Add");
        c.gridx = 2;
        c.gridy = 6;
        c.insets = new Insets(20,10,0,0);
        c.anchor = GridBagConstraints.WEST;
        panelForm.add(this.btnAddDriveLicense, c);

        this.btnRemoveDriveLicense = new JButton("Remove");
        c.gridx = 2;
        c.gridy = 7;
        c.insets = new Insets(5,10,10,0);
        c.anchor = GridBagConstraints.WEST;
        panelForm.add(this.btnRemoveDriveLicense, c);

        this.cbDrvLicense = new JComboBox<>(DrivingLicense.values());
        c.gridx = 1;
        c.gridy = 7;
        c.insets = new Insets(5,10,10,0);
        c.anchor = GridBagConstraints.CENTER;
        panelForm.add(this.cbDrvLicense, c);

        panBtnAddVehicle = new JPanel(new FlowLayout(FlowLayout.LEFT));

        panelBtn.add(panBtnAddVehicle);
        panelBtn.add(btnRemove);
        panelBtn.add(btnSaveModify);

        this.add(panelTitle, BorderLayout.NORTH);
        this.add(panelForm, BorderLayout.CENTER);
        this.add(this.panelBtn, BorderLayout.SOUTH);

        prevPanel.setVisibleBtnBackListener(true, e -> prevPanel.setPanel(new StoragePerson(prevPanel, model)));

    }

    @Override
    public int showOptionDialog(String text){
        int result = JOptionPane.showOptionDialog(null, text, "Question...", JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE, null, null, null);

        return result;
    }

    @Override
    public void message(String text){
        JOptionPane.showMessageDialog(this, text);
    }

}
