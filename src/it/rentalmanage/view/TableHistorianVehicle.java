package it.rentalmanage.view;

import it.rentalmanage.controller.ITableHistorianVehicleController;
import it.rentalmanage.controller.TableHistorianVehicleController;
import it.rentalmanage.model.IVehicle;
import it.rentalmanage.model.IModel;

import javax.swing.*;

/**
 * Tabella che mostra lo storico dei clienti che hanno noleggiato un veicolo
 * @author nicolapanigucci
 */
public class TableHistorianVehicle extends JScrollPane {

    private JTable tbHistorian;
    private ITableHistorianVehicleController tableHistorianVehicleController;
    private ICTMHistorianVehicle customTableModelHistorian;

    /**
     * Costruttore di TableHistorianVehicle
     * @param prevPanel oggetto di tipo MainFrame
     * @param model oggetto di tipo IModel
     * @param vehicle oggetto di tipo IVehicle. Veicolo di cui si vuole visualizzare lo storico
     */
    public TableHistorianVehicle(final MainFrame prevPanel, final IModel model, final IVehicle vehicle) {

        customTableModelHistorian = new CTMHistorianVehicle(model);
        this.tbHistorian = new JTable(customTableModelHistorian);
        this.tableHistorianVehicleController = new TableHistorianVehicleController(model, customTableModelHistorian);
        tableHistorianVehicleController.showHistorianVehicle(vehicle);
        tbHistorian.setFillsViewportHeight(true);

        this.setViewportView(tbHistorian);

        revalidate();
    }
}
