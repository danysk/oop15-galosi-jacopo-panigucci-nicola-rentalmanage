package it.rentalmanage.it.rentalmanage.test;

import it.rentalmanage.model.*;
import it.rentalmanage.model.filemanager.IFileManager;
import it.rentalmanage.model.filemanager.JSonFileManager;

import java.util.*;

/**
 * Created by utente on 28/05/2016.
 */
public class Test {


    public void twoUserTwoVehiclesTest(List<IPerson> personList, List<IVehicle> carList){
        List<DrivingLicense> drivingLicenseList = new LinkedList<>();
        drivingLicenseList.add(DrivingLicense.B);
        Calendar date = new GregorianCalendar(1993, 2, 25);
        IFileManager manager = new JSonFileManager();
        IPerson person1 = new Person(new LinkedList<IRentedVehiclePeriod>(),"1234567890ABCEFG","1234567", date.getTime(),"Via Popilia 174",
                "Galosi","Jacopo",drivingLicenseList);
        date = new GregorianCalendar(1990, 5, 10);
        IPerson person2 = new Person(new LinkedList<IRentedVehiclePeriod>(),"1234567890AAAAAA","7654321", date.getTime(),"Via dei pini 4",
                "Panigucci", "Nicola",drivingLicenseList);
        IVehicle car = new Vehicle(new LinkedList<IRentalPeriod>(),new Date(),125,new Date(),250,90,DrivingLicense.B,4,new Date(),700,
                "AA777AA","Fiat","Punto",true);
        IVehicle motorbike = new Vehicle(new LinkedList<IRentalPeriod>(),new Date(),200,new Date(),350,70,DrivingLicense.A1,2,new Date(),1500,
                "BB555CC","Ducati","999",true);
        if(personList.isEmpty()) {
            personList.add(person1);
            personList.add(person2);
            manager.writeList(manager.writePersonToJArray(personList),"Person");
        }
        if(carList.isEmpty()){
            carList.add(car);
            carList.add(motorbike);
            manager.writeList(manager.writeVehicleToJArray(carList),"Vehicle");
        }
    }

    public void stressTest(List<IPerson> personList, List<IVehicle> vehicleList){
        List<DrivingLicense> drivingLicensesList = new LinkedList<>();
        drivingLicensesList.add(DrivingLicense.B);
        for(int i = 0; i<100000; i++){
            String fcode = "1234567AABBQQWW"+i;
            String phoneNumb = "1243567";
            Calendar bDay = new GregorianCalendar(1990, 10, 15);
            String address = "Via Popilia" + i;
            IPerson person = new Person(new LinkedList<IRentedVehiclePeriod>(),fcode,phoneNumb,bDay.getTime(),address,"Mario","Rossi",drivingLicensesList);
            personList.add(person);
        }
        IFileManager manager = new JSonFileManager();
        manager.writeList(manager.writePersonToJArray(personList),"Person");
        for(int i = 0; i< 100000; i++){
            Date genericDate = new Date();
            int genericCost = 150;
            int seatsNumber = 5;
            String numberPlate = "A"+i;
            String manufactorer = "Fiat";
            String model = "Tipo";
            IVehicle vehicle = new Vehicle(new LinkedList<IRentalPeriod>(),genericDate,genericCost,genericDate,genericCost,genericCost,DrivingLicense.B,seatsNumber,
                    genericDate,genericCost,numberPlate,manufactorer,model,true);
            vehicleList.add(vehicle);
        }
        manager.writeList(manager.writeVehicleToJArray(vehicleList),"Vehicle");
    }

}
