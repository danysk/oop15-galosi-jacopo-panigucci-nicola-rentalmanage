package it.rentalmanage.model;

import java.util.*;

/**
 * Implementazione dell'interfaccia IPerson
 * @author Jacopo Galosi
 * @see it.rentalmanage.model.IPerson
 */
public class Person  implements IPerson {

    private String name;
    private String surname;
    private String address;
    private Date birthDate;
    private String phoneNumber;
    private String fiscalCode;
    private List<DrivingLicense> licencesList;
    private final Collection<IRentedVehiclePeriod> rentedCarList;

    /**
     * Costruttore di Person
     * @param carPeriodList Collection di periodi di noleggio
     * @param fiscalCode codice fiscale della persona
     * @param phoneNumber numero di telefono della persona
     * @param birthDate data di nascita della persona
     * @param address indirizzo della persona
     * @param surname cognome della persona
     * @param name nome della persona
     * @param licencesList lista di patenti possedute dalla persona
     */
    public Person(Collection<IRentedVehiclePeriod> carPeriodList, String fiscalCode, String phoneNumber, Date birthDate, String address, String surname, String name, List<DrivingLicense> licencesList) {
        if (carPeriodList == null || fiscalCode == null || phoneNumber == null || birthDate == null || address == null || surname == null || name == null || licencesList == null) {
            throw new NullPointerException();
        }

        this.rentedCarList = carPeriodList;
        this.licencesList = new LinkedList<>(licencesList);
        this.fiscalCode = fiscalCode;
        this.phoneNumber = phoneNumber;
        this.birthDate = birthDate;
        this.address = address;
        this.surname = surname;
        this.name = name;
    }

    @Override
    public void setName(String name) {
        this.name = name;


    }

    @Override
    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getSurname() {
        return this.surname;
    }

    @Override
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;

    }

    @Override
    public String getPhoneNumber() {
        return this.phoneNumber;
    }


    @Override
    public String getFiscalCode() {
        return this.fiscalCode;
    }

    @Override
    public String getAddress() {
        return this.address;
    }

    @Override
    public void setAddress(String address) {
        this.address = address;

    }

    @Override
    public void setDrivingLicense(List<DrivingLicense> drivingLicenses) {
        this.licencesList = drivingLicenses;
    }

    @Override
    public List<DrivingLicense> getDrivingLicense() {
        List<DrivingLicense> list = new LinkedList<>(licencesList);
        return list;

    }

    @Override
    public Date getBirthDate() {
        return this.birthDate;
    }


    @Override
    public Collection<IRentedVehiclePeriod> getAllRentedVehicles() {
        return new ArrayList<>(this.rentedCarList);
    }

    @Override
    public void addCar(IRentedVehiclePeriod carPeriod) {
        if (carPeriod == null) {
            throw new NullPointerException();
        }
        rentedCarList.add(carPeriod);

    }
}

