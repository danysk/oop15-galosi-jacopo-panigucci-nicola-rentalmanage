package it.rentalmanage.model;

import java.util.*;

/**
 * Implementazione dell'interfaccia IModel
 * @author Jacopo Galosi
 * @see it.rentalmanage.model.IModel
 */
public class Model implements IModel {

    private final Map<String ,IPerson> personMap;
    private final Map<String,IVehicle> vehicleMap;
    private final Map<String,IPerson> personHistory;
    private final Map<String,IVehicle> vehicleHistory;

    /**
     * Costruttore di Model
     * @param personList lista con tutte le persone inserite all'interno del programma
     * @param vehiclesList lista con tutte le auto inserite all'interno del programma
     * @param personHistoryCol storico di tutte le persone con almeno 1 noleggio
     * @param vehiclesHistoryCol storico di tutte le auto che sono state noleggiate almeno una volta
     */
    public Model(Collection<IPerson> personList, Collection<IVehicle> vehiclesList, Collection<IPerson> personHistoryCol, Collection<IVehicle> vehiclesHistoryCol) {
        if(personList == null || vehiclesList == null || personHistoryCol == null || vehiclesHistoryCol == null){
            throw new IllegalArgumentException();
        }
        this.personMap = new HashMap<>();
        for (IPerson iPerson : personList) {
            // controllo duplicati
            if(personMap.put(iPerson.getFiscalCode(),iPerson)!= null){
                throw new IllegalArgumentException();
            }
        }
        this.vehicleMap = new HashMap<>();
        for (IVehicle iVehicle : vehiclesList) {
            if(vehicleMap.put(iVehicle.getNumberPlate(), iVehicle)!= null){
                throw new IllegalArgumentException();
            }
        }
        this.personHistory = new HashMap<>();
        for (IPerson iPerson : personHistoryCol) {
            if(personHistory.put(iPerson.getFiscalCode(),iPerson)!= null){
                throw new IllegalArgumentException();
            }
        }
        this.vehicleHistory = new HashMap<>();
        for (IVehicle car : vehiclesHistoryCol) {
            if(vehicleHistory.put(car.getNumberPlate(),car)!= null){
                throw new IllegalArgumentException();
            }
        }

    }

    @Override
    public Map<String, IPerson> getAllPersons() {
        return new HashMap<>(this.personMap);
    }

    @Override
    public Map<String, IVehicle> getAllCar() {
        return new HashMap<>(this.vehicleMap);

    }

    @Override
    public Map<String, IPerson> getAllPersonsHistory() {
        return new HashMap<>(this.personHistory);
    }

    @Override
    public IPerson getPersonFromFCodeHistory(String fCode) {
        Map<String, IPerson> map = new HashMap<>(getAllPersonsHistory());
        if (!map.containsKey(fCode)){
            throw new IllegalArgumentException();
        }
        return map.get(fCode);
    }

    @Override
    public IVehicle getCarFromNumPlateHistory(String numberPlate) {
        Map<String,IVehicle> map = new HashMap<>(getAllCarsHistory());
        if (!map.containsKey(numberPlate)){
            throw new IllegalArgumentException();
        }
        return map.get(numberPlate);
    }

    @Override
    public Map<String, IVehicle> getAllCarsHistory() {
        return new HashMap<>(this.vehicleHistory);
    }

    @Override
    public void addPerson(IPerson person) {
        if(person == null){
            throw new NullPointerException();
        }
        personMap.put(person.getFiscalCode(),person);
    }

    @Override
    public void addPersonToHistory(IPerson person) {
        if(person == null){
            throw new NullPointerException();
        }
        personHistory.put(person.getFiscalCode(),person);
    }

    @Override
    public void removePerson(IPerson person) {
        if(person == null){
            throw new NullPointerException();
        }
        if(!personMap.containsKey(person.getFiscalCode())){
            throw new IllegalArgumentException();
        }
        personMap.remove(person.getFiscalCode());
    }

    @Override
    public void addCar(IVehicle car) {
       if(car == null){
            throw new NullPointerException();
        }
        vehicleMap.put(car.getNumberPlate(),car);
    }

    @Override
    public void addCarToHistory(IVehicle car) {
        if(car == null){
            throw new NullPointerException();
        }
        vehicleHistory.put(car.getNumberPlate(),car);
    }

    @Override
    public void removeCar(IVehicle car) {
        if(car == null){
            throw new NullPointerException();
        }
        if(!vehicleMap.containsKey(car.getNumberPlate())){
            throw new IllegalArgumentException();
        }
        vehicleMap.remove(car.getNumberPlate());
    }




}
