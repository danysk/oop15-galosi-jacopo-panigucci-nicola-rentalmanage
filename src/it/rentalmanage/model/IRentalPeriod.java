package it.rentalmanage.model;

import java.util.Date;

/**
 * Modella il periodo di noleggio da salvare dentro l'oggetto auto mantenendo in memoria il codice
 * fiscale della persona che ha noleggiato l'auto e in quale periodo l'ha noleggiata.
 * Una sua implementazione è RentalPeriod
 * @author Jacopo Galosi
 */
public interface IRentalPeriod {

    /**
     *
     * @return chi ha noleggiato l'auto
     */
    String getFCode();

    /**
     *
     * @return la data di inizio noleggio
     */
    Date getStartDate();

    /**
     * Imposta la data di inizio noleggio
     * @param startDate  data di inizio nolettio
     */
    void setStartDate(Date startDate);

    /**
     * Ritorna la data di fine noleggio
     * @return data di fine noleggio
     */
    Date getEndDate();

    /**
     * Imposta la data di fine noleggio, può essere null
     * @param endDate  la data di fine noleggio
     */
    void setEndDate(Date endDate);


}
