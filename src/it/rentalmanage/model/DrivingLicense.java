package it.rentalmanage.model;

/**
 * enum di tutte le patenti supportate dal programma RentalManage
 * @author Jacopo Galosi
 */
public enum DrivingLicense {


    A1, A2, A3, B, C, D, BE, CE, DE
}
