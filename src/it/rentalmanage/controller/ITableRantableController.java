package it.rentalmanage.controller;

/**
 * Implementa il metodi che permette di visualizzare tutti i veicoli noleggiabili
 * @author nicolapanigucci
 */
public interface ITableRantableController {

    /**
     * Visualizza tutti i veicoli noleggiabili
     */
    void showRentableVehicle();
}
