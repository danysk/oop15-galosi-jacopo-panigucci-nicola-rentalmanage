package it.rentalmanage.controller;

import it.rentalmanage.model.DrivingLicense;
import it.rentalmanage.model.IVehicle;

/**
 * Implementa il metodo che gestisce il noleggio di un veicolo.
 * @author nicolapanigucci
 */
public interface IRentController {

    /**
     * Verifica che il cliente possieda la stessa patente (o superiore) richiesta dal veicolo
     * @param drivingLicenseRequired
     * @return true se il cliente possiede la stessa patente o superiore
     */
    boolean canRentVehicle(final DrivingLicense drivingLicenseRequired);

    /**
     * Gestisce ill noleggio di un cliente
     * @param vehicle oggetto di tipo IVehicle. Veicolo che si vuole noleggiare
     * @param endDate data di fine noleggio
     * @param choice scelta dell'utente
     * @return 0 se l'utente non vuole continuare l'operazione
     *  -1 se l'utente vuole continuare l'operazione, ma la data inserita non è valida
     *  1 se l'operazione va a buon fine
     */
    int rent(final IVehicle vehicle, String endDate, final int choice);

}
