package it.rentalmanage.controller;

import it.rentalmanage.model.DrivingLicense;
import it.rentalmanage.view.MainFrame;
import it.rentalmanage.view.ModifyVehicle;

import java.util.Date;

/**
 * Implementa i metodi che gestiscono la modifica e l'eliminazione di un veicolo
 * @author nicolapanigucci
 */
public interface IVehicleController {

    /**
     * Aggiorna i dati di un veicolo e li salva nei file
     * @param rentPrice prezzo di noleggio
     * @param requestedLicence patente di guida richiesta per guidare il veicolo
     * @param insuranceCost costo dell'assicurazione
     * @param insuranceExpiring data di cadenza dell'assicurazione
     * @param carTaxCost costo del bollo
     * @param carTaxExpiring data di scadenza del bollo
     * @param MOOTestCost costo della revisione
     * @param MOOTestExpiring data di scadenza della revisione
     */
    void updateVehicle(int rentPrice, DrivingLicense requestedLicence, int insuranceCost, Date insuranceExpiring,
                       int carTaxCost, Date carTaxExpiring, int MOOTestCost, Date MOOTestExpiring);

    /**
     * Elimina il veicolo dai file
     * @param prevPanel oggetto di tipo MainFrame
     * @param modifyVehicle oggetto di tipo ModifyVehicle
     * @param choice scelta dell'utente (NO, YES)
     */
    void deleteVehicle(final MainFrame prevPanel, final ModifyVehicle modifyVehicle, final int choice);

}
