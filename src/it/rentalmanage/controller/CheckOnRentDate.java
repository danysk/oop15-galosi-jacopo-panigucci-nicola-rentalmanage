package it.rentalmanage.controller;

import it.rentalmanage.view.TableRentVehicle;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Verifica la validità della data di fine noleggio
 * @author nicolapanigucci
 * @see it.rentalmanage.controller.ICheckOnRentDate
 */
public class CheckOnRentDate implements ICheckOnRentDate {

    private SimpleDateFormat sdf;
    private Calendar currentDate;
    private Date endDate;
    private TableRentVehicle view;

    /**
     * Costruttore di CheckOnRentDate
     * @param view
     */
    public CheckOnRentDate(final TableRentVehicle view){
        this.sdf = new SimpleDateFormat("dd/MM/yyyy");
        this.sdf.setLenient(false);
        this.currentDate = Calendar.getInstance();
        this.currentDate.setTime(new Date());

        this.view = view;
    }

    @Override
    public boolean rentDate(String date) {

        Date dateValid ;
        Calendar cal ;

        if (date.length() > 10){
            view.message("Wrong format date!");
            return  false;
        }

        try {
            dateValid = sdf.parse(date);
            cal = Calendar.getInstance();
            cal.setTime(dateValid);

            if (!currentDate.before(cal)){ //se start viene prima di cal
                view.message("The date isn't later than the current!");
                return false;
            } else if ((cal.get(Calendar.YEAR) - currentDate.get(Calendar.YEAR)) > 20) { //il noleggio è al massimo di 20 anni
                view.message("The rental must be less than 20 years!");
                return false;
            } else {
                this.endDate = cal.getTime();
                return true;
            }

        } catch (ParseException e) {
            view.message("Wrong format date!");
            return false;
        }

    }

    @Override
    public Date getCurrentDate() {
        return this.currentDate.getTime();
    }

    @Override
    public Date getEndDate() {
        return this.endDate;
    }
}
