package it.rentalmanage.controller;

import it.rentalmanage.model.IPerson;

/**
 * Implementa il metodo che permette di visualizzare lo storico dei veicoli noleggiati da un cliente
 * @author nicolapanigucci
 */
public interface ITableHistorianPersonController {

    /**
     * Visualizza lo storico dei veicoli noleggiati da 'person' in una tabella
     * @param person oggetto di tipo IPerson. Cliente che ha noleggiato i veicoli
     */
    void showHistorianPerson(final IPerson person);
}
