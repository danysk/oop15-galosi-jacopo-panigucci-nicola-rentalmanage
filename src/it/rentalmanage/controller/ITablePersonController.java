package it.rentalmanage.controller;

/**
 * Interfaccia del controller per la visualizzazione della tabella delle persone
 * @author Jacopo Galosi
 */
public interface ITablePersonController {

    /**
     * Visualizza le persone in una tabella
     */
    void showPerson();


}
