package it.rentalmanage.controller;

import it.rentalmanage.model.DrivingLicense;
import it.rentalmanage.view.FormPerson;

import javax.swing.*;
import java.util.List;

/**
 * Gestisce l'aggiunta e la rimozione delle patenti con le relative dipendenze, verificando inoltre se l'utente ha l'età giusta
 * @author nicolapanigucci
 * @see it.rentalmanage.controller.IFormPersonController
 */
public class FormPersonController implements IFormPersonController {

    private ICheckOnDate checkOnDate;
    private FormPerson view;

    /**
     * Costruttore di FormPersonController
     * @param view
     */
    public FormPersonController(final FormPerson view){
        this.checkOnDate = new CheckOnDate(view);
        this.view = view;
    }

    @Override
    public List<DrivingLicense> addDriveLicenseToList(List<DrivingLicense> allDLicense, final DrivingLicense d, final String birthDate) {

        if (checkOnDate.check16YearsOld(birthDate) && !allDLicense.contains(d)){
            if (d == DrivingLicense.C || d == DrivingLicense.CE ||
                    d == DrivingLicense.D || d == DrivingLicense.DE){

                int result = -1;
                if (!allDLicense.contains(DrivingLicense.B)){
                    result = view.showOptionDialog("Required B licence! Do you have it?");
                }

                if (result == JOptionPane.YES_OPTION || allDLicense.contains(DrivingLicense.B)){
                    if (!allDLicense.contains(DrivingLicense.B) && checkOnDate.check18YearsOld(birthDate)){
                        allDLicense.add(DrivingLicense.B);
                    }
                    if ((d == DrivingLicense.C || d == DrivingLicense.CE) &&
                            checkOnDate.check21YearsOld(birthDate)){
                        allDLicense.add(d);
                    }
                    if ((d == DrivingLicense.D || d == DrivingLicense.DE) &&
                            checkOnDate.check24YearsOld(birthDate)){
                        allDLicense.add(d);
                    }
                }

            } else {
                //per le patenti A1, A2, A3, B, BE
                if (d == DrivingLicense.A1){
                    allDLicense.add(d);
                } else if (checkOnDriveLicence(d, birthDate)){
                    allDLicense.add(d);
                }
            }

        }

        return allDLicense;

    }

    @Override
    public List<DrivingLicense> removeDriveLicenseFromList(List<DrivingLicense> allDLicense, final List<DrivingLicense> specificList, final DrivingLicense d) {

        if (d == DrivingLicense.B && (allDLicense.contains(DrivingLicense.C) ||
                allDLicense.contains(DrivingLicense.CE) || allDLicense.contains(DrivingLicense.D) ||
                allDLicense.contains(DrivingLicense.DE))){

            int result = view.showOptionDialog("Some driving licence require B! Do you want erase them?");

            if (result == JOptionPane.YES_OPTION){
                allDLicense.removeAll(specificList);
            }
        }else {
            allDLicense.remove(d);
        }

        return allDLicense;

    }

    /**
     * Verifica se la persona ha l'età giusta per possedere le patenti A2, A3, B, BE
     * @param drivingLicense patente selezionata
     * @param date data di nascita
     * @return true se la persona ha l'età giusta
     */
    private boolean checkOnDriveLicence(final DrivingLicense drivingLicense, final String date){
        boolean flag = false;

        if (drivingLicense == null){
            return false;
        }

        if ((drivingLicense == DrivingLicense.A2 || drivingLicense == DrivingLicense.B || drivingLicense == DrivingLicense.BE) &&
                checkOnDate.check18YearsOld(date)){
            flag = true;
        }

        if (drivingLicense == DrivingLicense.A3 &&
                checkOnDate.check24YearsOld(date)){
            flag = true;
        }

        return flag;
    }

}
