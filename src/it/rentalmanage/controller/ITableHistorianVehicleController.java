package it.rentalmanage.controller;

import it.rentalmanage.model.IVehicle;

/**
 * Implementa il metodo che permette di visualizzare lo storico dei clienti che hanno noleggiato un veicolo
 * @author nicolapanigucci
 */
public interface ITableHistorianVehicleController {

    /**
     * Visualizza lo storico dei clienti che hanno noleggiato 'vehicle' in una tabella
     * @param vehicle oggetto di tipo IVehicle. Veicolo noleggiato
     */
    void showHistorianVehicle(IVehicle vehicle);
}
