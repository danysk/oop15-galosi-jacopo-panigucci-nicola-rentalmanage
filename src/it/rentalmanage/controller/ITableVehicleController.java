package it.rentalmanage.controller;

/**
 * Interfaccia per il controller che permette di visualizzare la tabella dei veicoli
 * @author Jacopo Galosi
 *
 */
public interface ITableVehicleController {

    /**
     * Permette di visualizzare i veicoli
     */
    void showVehicle();
}
