package it.rentalmanage.controller;

import it.rentalmanage.model.IPerson;

/**
 * Interfaccia del controller per l'aggiunta di una persona
 * @author Jacopo Galosi
 */
public interface IAddPersonController {

    /**
     * Scrive una persona su file
     * @param person
     */
    void writePerson(IPerson person);

}
