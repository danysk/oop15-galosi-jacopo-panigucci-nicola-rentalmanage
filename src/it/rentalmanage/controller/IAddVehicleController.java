package it.rentalmanage.controller;

import it.rentalmanage.model.IVehicle;

/**
 * Interfaccia del controller per l'aggiunta di un nuovo veicolo
 * @author Jacopo Galosi
 */
public interface IAddVehicleController {

    /**
     * Scrive un veicolo su file
     * @param vehicle oggetto di tipo veicolo
     */
    void writeVehicle(IVehicle vehicle);
}
