package it.rentalmanage.controller;

import java.util.Calendar;
import java.util.Date;

/**
 * Implementa i metodi che controllano la validità della data di nascita e l'età di una persona
 * @author nicolapanigucci
 */
public interface ICheckOnDate {

    /**
     * Verifica il requisito necessario per essere un cliente
     * @param date data di nascita della persona
     * @return true se ha almeno 16 anni
     */
    boolean check16YearsOld(String date);

    /**
     * Verifica se la persona ha almeno 18 anni. Requisito per possedere le patenti A2 e B
     * @param date data di nascita della persona
     * @return true se la persona ha almeno 18 anni
     */
    boolean check18YearsOld(String date);

    /**
     * Verifica se la persona ha almeno 21 anni. Requisito per possedere le patenti C e CE
     * @param date data di nascita della persona
     * @return true se la persona ha almeno 21 anni
     */
    boolean check21YearsOld(String date);

    /**
     * Verifica se la persona ha almeno 24 anni. Requisito per possedere le patenti A3, D o DE
     * @param date data di nascita della persona
     * @return true se la persona ha almeno 24 anni
     */
    boolean check24YearsOld(String date);

    /**
     * Controlla la validità di una data
     * @param date data da controllare
     * @return true se la data è valida
     */
    Calendar checkBirthDate(String date);

    /**
     * Calcola l'età di una persona
     * @param date data di nascita di una persona
     * @return età della persona, altrimenti -1 se 'bDay' non è una data di nascita valida
     */
    int age(String date);

}
