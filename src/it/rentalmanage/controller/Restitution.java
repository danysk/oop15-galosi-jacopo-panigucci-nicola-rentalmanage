package it.rentalmanage.controller;

import it.rentalmanage.model.IModel;
import it.rentalmanage.model.IPerson;
import it.rentalmanage.model.IRentalPeriod;
import it.rentalmanage.model.IRentedVehiclePeriod;
import it.rentalmanage.model.filemanager.IFileManager;
import it.rentalmanage.model.filemanager.JSonFileManager;

import javax.swing.*;
import java.util.Calendar;
import java.util.Date;

/**
 * Gestisce la restituzione di un veicolo
 * @author nicolapanigucci
 * @see it.rentalmanage.controller.IRestitution
 */
public class Restitution implements IRestitution {

    private IModel model;
    private IPerson person;
    private IRentedVehiclePeriod rentedVehiclePeriod;
    private Calendar calendar;

    /**
     * Costruttore di Restitution
     * @param model oggetto di tipo IModel
     * @param person oggetto di tipo IPerson. Cliente che vuole restituire un veicolo noleggiato
     * @param rentedVehiclePeriod oggetto fi tipo IRentedVehiclePeriod
     */
    public Restitution (final IModel model, final IPerson person, final IRentedVehiclePeriod rentedVehiclePeriod){
        this.model = model;
        this.person = person;
        this.rentedVehiclePeriod = rentedVehiclePeriod;
        this.calendar = Calendar.getInstance();
    }

    @Override
    public void restitution(final int choice) {

        if (choice == JOptionPane.YES_OPTION){

            restitutionVehicle();

        }

    }

    @Override
    public boolean isDeliveredVehicle(){
        return this.model.getCarFromNumPlateHistory(this.rentedVehiclePeriod.getNumberPlate()).isRentable();
    }

    /**
     * Gestisce la restituzione di un veicolo nei file
     */
    private void restitutionVehicle() {

        this.calendar.setTime(new Date());

        this.model.getAllCar().get(this.rentedVehiclePeriod.getNumberPlate()).setRentability(true);
        this.model.getAllCarsHistory().get(this.rentedVehiclePeriod.getNumberPlate()).setRentability(true);

        for (IRentalPeriod rentalPeriod : this.model.getAllCar().get(this.rentedVehiclePeriod.getNumberPlate()).getAllTenant()){
            if (rentalPeriod.getFCode().equals(this.person.getFiscalCode())){
                rentalPeriod.setEndDate(this.calendar.getTime());
            }
        }
        this.rentedVehiclePeriod.setEndDate(this.calendar.getTime());

        IFileManager fileManager = new JSonFileManager();
        fileManager.writeList(fileManager.writeVehicleToJArray(model.getAllCar().values()), "Vehicle");
        fileManager.writeList(fileManager.writePersonToJArray(model.getAllPersons().values()), "Person");
        fileManager.writeList(fileManager.writePersonToJArray(model.getAllPersonsHistory().values()), "PersonHistory");
        fileManager.writeList(fileManager.writeVehicleToJArray(model.getAllCarsHistory().values()), "VehicleHistory");
    }
}
