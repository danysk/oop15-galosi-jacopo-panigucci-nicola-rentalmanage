package it.rentalmanage.controller;

import it.rentalmanage.model.IVehicle;
import it.rentalmanage.model.IModel;
import it.rentalmanage.view.ICustomTableModelVehicle;

import java.util.Map;

/**
 * Implementazione dell'interfaccia ITableVehicleController
 * @author Jacopo Galosi
 * @see it.rentalmanage.controller.ITableVehicleController
 */
public class TableVehicleController implements ITableVehicleController {

    private IModel model;
    private ICustomTableModelVehicle customTableModelVehicle;

    /**
     * Costruttore di TableVehicleController
     * @param model oggetto di tipo model
     * @param viewModel oggetto di tipo table model vehicle
     */
    public TableVehicleController(final IModel model, final ICustomTableModelVehicle viewModel) {
        this.model = model;
        this.customTableModelVehicle = viewModel;
    }

    @Override
    public void showVehicle() {
        Map<String,IVehicle> carMap = model.getAllCar();
        customTableModelVehicle.setElement(carMap);
    }
}
