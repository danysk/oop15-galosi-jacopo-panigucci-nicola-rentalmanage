package it.rentalmanage.controller;

import it.rentalmanage.view.FormPerson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Verifica la validità delle date sulle patenti e sulla persona
 * @author nicolapanigucci
 * @see it.rentalmanage.controller.ICheckOnDate
 */
public class CheckOnDate implements ICheckOnDate {

    private SimpleDateFormat sdf;
    private Calendar currentDate;
    private FormPerson view;

    /**
     * Costruttore di CheckOnDate
     * @param view
     */
    public CheckOnDate(final FormPerson view){
        this.sdf = new SimpleDateFormat("dd/MM/yyyy");
        this.sdf.setLenient(false);
        this.currentDate = Calendar.getInstance();
        this.currentDate.setTime(new Date());

        this.view = view;
    }

    @Override
    public boolean check16YearsOld(String date) {

        int age = age(date);

        if (age == -1){
            this.view.message("Wrong date format!");
            return false;
        } else if (age < 16 || age >= 100){
            this.view.message("Too young or too old!");
            return false;
        } else{
            return true;
        }
    }

    @Override
    public boolean check18YearsOld(String date) {

        int age = age(date);

        if (age == -1){
            this.view.message("Wrong date format!");
            return false;
        }
        if (age < 18 || age >= 100){
            this.view.message("Too young or too old!");
            return false;
        } else {
            return true;
        }
    }

    @Override
    public boolean check21YearsOld(String date) {

        int age = age(date);

        if (age == -1){
            this.view.message("Wrong date format!");
            return false;
        } else if (age < 21 || age >= 100){
            this.view.message("Too young or too old!");
            return false;
        } else {
            return true;
        }
    }

    @Override
    public boolean check24YearsOld(String date) {

        int age = age(date);

        if (age == -1){
            this.view.message("Wrong date format!");
            return false;
        } else if (age < 24 || age >= 100){
            this.view.message("Too young or too old!");
            return false;
        } else {
            return true;
        }

    }

    @Override
    public Calendar checkBirthDate(String date) {

        if (date.length() > 10){
            return null;
        }

        try {

            Date dateValid = sdf.parse(date);
            Calendar cal = Calendar.getInstance();
            cal.setTime(dateValid);

            return cal;

        } catch (ParseException e) {
            return null;
        }
    }

    @Override
    public int age(String date) {

        Calendar bDay = checkBirthDate(date);

        if (bDay != null) {

            int yBday = bDay.get(Calendar.YEAR);
            int yToday = currentDate.get(Calendar.YEAR);
            return yToday - yBday;
        } else{
            return -1;
        }

    }

}
