package it.rentalmanage.controller;

import it.rentalmanage.model.DrivingLicense;
import it.rentalmanage.view.MainFrame;
import it.rentalmanage.view.ModifyPerson;

import java.util.List;

/**
 * Interfaccia del controller per la modifica/eliminazione di una persona
 * @author Jacopo Galosi
 */
public interface IPersonController {

    /**
     * Aggiorna i dati di una persona e li salva nei file
     */
    void updatePerson(String surname, String name, String address, String telephone, List<DrivingLicense> drivingLicenses);

    /**
     * Elimina la persona dai file
     */
    void deletePerson(final MainFrame prevPanel, final ModifyPerson modifyPerson, final int choice);
}
