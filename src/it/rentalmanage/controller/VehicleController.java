package it.rentalmanage.controller;

import it.rentalmanage.model.DrivingLicense;
import it.rentalmanage.model.IVehicle;
import it.rentalmanage.model.IModel;
import it.rentalmanage.model.filemanager.IFileManager;
import it.rentalmanage.model.filemanager.JSonFileManager;
import it.rentalmanage.view.MainFrame;
import it.rentalmanage.view.ModifyVehicle;
import it.rentalmanage.view.StorageVehicle;

import javax.swing.*;
import java.util.Date;

/**
 * Gestisce la modifica e l'eliminazione di un veicolo
 * @author nicolapanigucci
 * @see it.rentalmanage.controller.IVehicleController
 */
public class VehicleController implements IVehicleController {

    private IFileManager manager;
    private IVehicle vehicle;
    private IModel model;

    /**Costruttore di VehicleController
    * @param model oggetto di tipo IModel
    * @param vehicle oggetto di tipo IVehicle. veicolo che si vuole modificare o eliminare
    */
    public VehicleController(final IModel model, final IVehicle vehicle){

        this.manager = new JSonFileManager();
        this.model = model;
        this.vehicle = vehicle;
    }

    @Override
    public void updateVehicle(int rentPrice, DrivingLicense requestedLicense, int insuranceCost, Date insuranceExpiring,
                              int carTaxCost, Date carTaxDate, int MOOTestCost, Date MOOTestExpiring) {
        this.vehicle.setRentPrice(rentPrice);
        this.vehicle.setRequestedLicence(requestedLicense);
        this.vehicle.setInsuranceCost(insuranceCost);
        this.vehicle.setInsuranceExpiring(insuranceExpiring);
        this.vehicle.setVehicleTaxCost(carTaxCost);
        this.vehicle.setVehicleTaxDate(carTaxDate);
        this.vehicle.setMotTestCost(MOOTestCost);
        this.vehicle.setMotTestDate(MOOTestExpiring);

        this.model.removeCar(vehicle);
        this.model.addCar(vehicle);
        manager.writeList(manager.writeVehicleToJArray(model.getAllCar().values()), "Vehicle");

    }

    @Override
    public void deleteVehicle(final MainFrame prevPanel, final ModifyVehicle modifyVehicle, final int choice){

        if (choice == JOptionPane.YES_OPTION){
            if (!this.vehicle.isRentable()){
                JOptionPane.showMessageDialog(modifyVehicle, "Error! this vehicle is rented");
                return;
            }else {
                this.model.removeCar(vehicle);
                manager.writeList(manager.writeVehicleToJArray(model.getAllCar().values()), "Vehicle");
            }

            prevPanel.setPanel(new StorageVehicle(prevPanel, this.model));

        }

    }

}
